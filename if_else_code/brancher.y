%{
#include <iostream>
#include <map>
#include <set>
#include <list>
#include <string>
#include <cstring>
#include <algorithm>
#include <vector>
#include <stack>

#include "Nodes.h"

extern "C" int yylex();
extern "C" int yyparse();

std::set<std::string> all_variables;
std::vector<TreeNode*> *lines = new std::vector<TreeNode*>();
std::vector<TreeNode*> *usedLines = new std::vector<TreeNode*>();
int block_index = 0;

void yyerror(const char *s);
void createNewExpr(ExprNode* lhs, ExprNode* rhs, char oper, ExprNode* newNode);
void declare_vars();
void declare_print();
void translate_lines(std::vector<TreeNode*> *lines);
void make_CFG(std::vector<TreeNode*> *lines, std::vector<TreeNode*> *usedLines);
void recurse_used(std::vector<CFGNode> *nodes, int index);

/* Helper function to concatenate two strings together */
char*concatenate(const char*a, const char*b) {
  char*result = new char[strlen(a) + strlen(b)];
  strcpy(result, a);
  strcpy(result+strlen(a), b);
  return result;
}

%}

%union {
  ExprNode* eNode;
  PrintNode* pNode;
  IfNode* ifNode;
  TreeNode* tNode;
  LogicNode* lNode;
  char* sVal;
}

%token NUM_I
%token NUM_F
%token VARNAME
%token ADD SUB MULT DIV EQUALS
%token LESS GREATER EQUALSS NOT_EQUALSS LESS_EQUALSS GREATER_EQUALSS
%token LPAREN RPAREN
%token WS
%token EOL
%token PRINT IF ELSE
%token LBRACE RBRACE
%token AND OR

%type<eNode> atomicExpr
%type<eNode> assign
%type<eNode> expr
%type<eNode> addSubExpr
%type<eNode> multDivExpr
%type<pNode> print
// TODO: if line node type?
%type<eNode> ifLine
%type<tNode> line
%type<tNode> multiLine
%type<eNode> booleanExpr 
%type<eNode> compareExpr

%type<sVal> NUM_I
%type<sVal> VARNAME
%type<sVal> PRINT
%type<sVal> IF ELSE
%type<sVal> LESS GREATER EQUALSS NOT_EQUALSS LESS_EQUALSS GREATER_EQUALSS COMPARE
%type<sVal> LBRACE RBRACE
%type<sVal> AND OR

%%

line: expr { $1->type = 'e'; lines->push_back($1); }
   | print { $1->type = 'p'; lines->push_back($1); }
   | ifLine { $1->type = 'i'; lines->push_back($1); }
    // This allows an empty line for in-lining in braces and blank lines 
   | { $$ = new ExprNode(); } 
   | LBRACE multiLine RBRACE { $$ = new ExprNode(); }
   ;

multiLine: multiLine EOL line { $$ = $3; $1->next = $3; }
         | line { }

// line: expr { $1->type = 'e'; }
//    | print { $1->type = 'p'; }
//    | ifLine { $1->type = 'i'; }
//    /* This allows an empty line for in-lining in braces and blank lines */
//    | { ExprNode* newNode = new ExprNode();  $$ = newNode; } 
//    | LBRACE multiLine RBRACE { ExprNode* newNode = new ExprNode(); $$ = newNode; }
//    ;
//
// multiLine: multiLine EOL line { $$ = $3; $1->next = $3; 
//             // NULL check the return of 'line' grammar rule 
//             // (because of the empty line clearance)
//             // std::cout << "1 PREV: " << $1->type << " NEXT: " << $1->next->type << std::endl;
//             if ($3->type != '\0')
//               lines->push_back($3); 
//            }
//          | line { 
//                   // std::cout << "2 PREV: " << $1->type << " NEXT: " << std::endl;
//                   if ($1->type != '\0')
//                     lines->push_back($1);
//                 }

expr: booleanExpr
    | assign

// TODO: re-work createNewExpr to also work for logicNode
booleanExpr: booleanExpr AND compareExpr {
              LogicNode* newNode = new LogicNode();
              newNode->lhs = $1;
              newNode->rhs = $3;
              newNode->compOp = $2;
              $$ = newNode;
             }
             | booleanExpr OR compareExpr {
              LogicNode* newNode = new LogicNode();
              newNode->lhs = $1;
              newNode->rhs = $3;
              newNode->compOp = $2;
              $$ = newNode;
             }
             | compareExpr

compareExpr: compareExpr COMPARE addSubExpr {
              LogicNode* newNode = new LogicNode();
              newNode->lhs = $1;
              newNode->rhs = $3;
              newNode->compOp = $2;
              $$ = newNode;
             }
             | addSubExpr

addSubExpr: addSubExpr ADD multDivExpr {
              ExprNode* newNode = new ExprNode;
              createNewExpr($1, $3, '+', newNode);
              $$ = newNode;
            }
          | addSubExpr SUB multDivExpr {
              ExprNode* newNode = new ExprNode;
              createNewExpr($1, $3, '-', newNode);
              $$ = newNode;
            }
          | multDivExpr

multDivExpr: multDivExpr MULT atomicExpr {
              ExprNode* newNode = new ExprNode;
              createNewExpr($1, $3, '*', newNode);
              $$ = newNode;
            }
          | multDivExpr DIV atomicExpr {
              if ($3 == 0) {
                std::cerr << "ERROR: Cannot divide by 0!" << std::endl;
                exit(1);
              }
              else {
                ExprNode* newNode = new ExprNode;
                createNewExpr($1, $3, '/', newNode);
                $$ = newNode;
              }
          }
          | atomicExpr

atomicExpr: VARNAME {
              ExprNode* newNode = new ExprNode;
              newNode->data = $1;
              $$ = newNode;
	      
      	      // add this variable to all_variables
      	      all_variables.insert(std::string($1));
            }
          | NUM_I { 
              ExprNode* newNode = new ExprNode;
	            newNode->data = concatenate("$", $1);
              $$ = newNode; 
            }
          | LPAREN expr RPAREN {
            $$ = $2;
          }

assign: VARNAME EQUALS expr {
          ExprNode* var = new ExprNode;
          var->data = $1;
          ExprNode* newNode = new ExprNode;
          createNewExpr(var, $3, '=', newNode);
          $$ = newNode;

          all_variables.insert(std::string($1));
        }

print: PRINT expr {
         PrintNode* print = new PrintNode;
         print->child = $2;

         $$ = print;
       }

COMPARE: LESS
       | GREATER
       | EQUALSS
       | NOT_EQUALSS
       | LESS_EQUALSS
       | GREATER_EQUALSS


// TODO: Construct if node type?
ifLine: IF LPAREN booleanExpr RPAREN {$$ = $3;}

%%

int main(int argc, char **argv) {
  yyparse();

  make_CFG(lines, usedLines);
  translate_lines(lines);
  declare_print();
  std::cout << "end: " << std::endl; // end of code label to not call sub-routines again
  declare_vars();

}

/* Display error messages */
void yyerror(const char *s) {
  fprintf(stderr, "ERROR: %s\n", s);
}

/* Assign expression node data */
void createNewExpr(ExprNode* lhs, ExprNode* rhs, char oper, ExprNode* newNode) {
  newNode->lhs = lhs;
  newNode->operation = oper;
  newNode->rhs = rhs;
}

/* Write asm data section for variables used in the script's language */
void declare_vars() {
  if (all_variables.size() > 0) {
    std::cout << std::endl << ".data " << std::endl;
    // each variable takes an entire register of indexing (32/64 bits) (e.g. .quad)
    for (auto iter=all_variables.begin(); iter != all_variables.end(); ++iter)
      std::cout << "    " << *iter << ":" << std::endl << "        .quad 0 " << std::endl;

    std::cout << std::endl; 
  }
}

/* Write asm print routine for outputting a number from our variables */
void declare_print() {
    // sub-routine for converting binary digit values to ascii value
    std::cout << "makeDigits: " << std::endl;    // label of makeDigits function
    std::cout << "xor %rdx, %rdx" << std::endl;  // clear rdx (dest of remainder)
    std::cout << "div %rbx" << std::endl;        // divide rax (wanted num to print) by 10
    std::cout << "mov %rax, %r8" << std::endl;   // save current result of div (remaining digits)
    std::cout << "mov %rdx, %rax" << std::endl;  // save remainder of div (next printed digit)
    std::cout << "add $0x30, %rax" << std::endl; // change value of div to ascii digit
    std::cout << "push %rax" << std::endl;       // push digit onto stack to be printed
    std::cout << "mov %r8, %rax" << std::endl;   // restore non-ascii value of rax
    std::cout << "inc %rcx" << std::endl;        // increment how many digits are made
    std::cout << "cmp $0, %rax" << std::endl;    // check if we're done making digits
    std::cout << "jnz makeDigits" << std::endl;  // move to next digit

    // sub-routine for outputting ascii bytes to the system.out
    std::cout << "printDigits:" << std::endl;    // label of printDigits function
    std::cout << "pop %rax" << std::endl;        // get next ascii char to print
    std::cout << "mov %rsp, %r9" << std::endl;   // save current stack pointer position
    std::cout << "mov %rcx, %r10" << std::endl;  // save current number of digits 
    std::cout << "sub $8, %rsp" << std::endl;    // move stack pointer to next byte
    std::cout << "mov %al, (%rsp)" << std::endl; // move lowest byte of val to the stack pointer
    std::cout << "mov %rsp, %rsi" << std::endl;  // move stack value to the source index
    std::cout << "mov $1, %rax" << std::endl;    // move 1 for syscall (system write)
    std::cout << "mov $1, %rdi" << std::endl;    // move 1 to dest index (system out)
    std::cout << "mov $1, %rdx" << std::endl;    // move 1 for number of bytes written (1)
    std::cout << "syscall" << std::endl;         // print
    std::cout << "mov %r9, %rsp" << std::endl;   // restore rsp (stack position)
    std::cout << "mov %r10, %rcx" << std::endl;  // restore rcx (number of digits left)
    std::cout << "dec %rcx" << std::endl;        // decrement number of digits left
    std::cout << "jnz printDigits" << std::endl; // continue print for more digits

    // print a a newline char (\n) at the end of our print 
    std::cout << "mov %rsp, %r9" << std::endl;   // save current stack pointer position
    std::cout << "mov $10, %rax" << std::endl;   // move ascii val of \n to output register
    std::cout << "sub $8, %rsp" << std::endl;    // move stack pointer to next byte
    std::cout << "mov %al, (%rsp)" << std::endl; // move lowest byte (\n) to the stack pointer
    std::cout << "mov $1, %rax" << std::endl;    // move 1 for syscall (system write)
    std::cout << "mov $1, %rdi" << std::endl;    // move 1 for dest index (system out)
    std::cout << "mov $1, %rdx" << std::endl;    // move 1 for number of bytes written (1)
    std::cout << "syscall" << std::endl;         // print
    std::cout << "mov %r9, %rsp" << std::endl;   // restore rsp (stack position)

    std::cout << "ret" << std::endl;             // return to where this subroutine was called
} 

/* Convert each line into appropriate assembly representation */
void translate_lines(std::vector<TreeNode*> *lines) {
  std::cout << ".globl main" << std::endl << std::endl << "main:"<< std::endl;

  for (int i=0; i<lines->size(); ++i)
    ((*lines)[i])->translate();

  // skip pass functionality sub-routines to the end of the program
  std::cout << "jmp end" << std::endl;
}

/* Creates graph structure to determine the dependencies each line
   requires to yield the proper information. Only run the necessary
   lines needed to yield requested data (e.g. PRINT statements)
*/
void make_CFG(std::vector<TreeNode*> *lines, std::vector<TreeNode*> *usedLines) {

  // define all sources and destinations by variable name 
  for (int j=0; j<lines->size(); ++j)
    ((*lines)[j])->start_DFS();

  std::vector<CFGNode>* nodes = new std::vector<CFGNode>(lines->size());

  // mark all predecessors and successors for a line by line number
  // iterate through each line j that uses variable source:
  for (int j=0; j<lines->size(); ++j) {
    for (auto source: (*((*lines)[j])->sources)) {
      // start searching for lines previous to j that modify
      // source. search backwards and break (below) on the first such
      // line that modifies source. 
      for (int i=j-1; i>=0; --i) {
        // Lines of the type X=Y+Z...
        if (((*lines)[i])->type == 'e') {
          ExprNode* eNode_ref = (ExprNode*) (*lines)[i];

      	  // If the destination of line i was a source of line j, then
      	  // link these lines.

      	  // Also, if that is the case, break on the i iteration,
      	  // because i was the most recent line with that dependency,
      	  // and so pure dependencies with other previous lines will
      	  // be out of date.
          if (strcmp(eNode_ref->dest, source) == 0) {
            (*nodes)[i].successors.insert(j);
            (*nodes)[j].predecessors.insert(i);
            break; // stop looking for previous predecessor
          }
        }
      }
    }
  }

  // mark lines used as predecessors of print lines
  for (int j=0; j<lines->size(); ++j) {
    if (((*lines)[j])->type == 'p')
      recurse_used(nodes, j);
    // always use block statements to check at runtime
    else if (((*lines)[j])->type == 'b')
      (*nodes)[j].not_dead_code = true;
  }

  // build vector of used lines
  for (int j=0; j<nodes->size(); ++j) {
    if ((*nodes)[j].not_dead_code == true)
      usedLines->push_back((*lines)[j]);
  }

}

/* For each line that is important (e.g., PRINT statements are by
   definition important), mark its predecessors in the graph as
   important:
*/
void recurse_used(std::vector<CFGNode> *nodes, int i) {
  (*nodes)[i].not_dead_code = true;

  for (auto predecessor: (*nodes)[i].predecessors) {
    recurse_used(nodes, predecessor);
  }
}
