#include <iostream>

typedef struct TreeNodeT {
      char type;

      virtual void translate() =0;
} TreeNode;

typedef struct ExprNodeT : TreeNode {

    char operation;
    char* data;

    ExprNodeT*lhs, *rhs;

    void translate() {
        if (this->lhs==NULL && this->rhs==NULL)
            // Base case: variable or integer value (both as string)
              std::cout << "push " << this->data << std::endl;
        else {
            // operations other than assign
            if (this->operation != '=') {
                // retrieve the last two values from the stack
                // to perform the operation on
                this->rhs->translate();
                std::cout << "pop %rax" << std::endl;
                this->lhs->translate();
                std::cout << "pop %rbx" << std::endl;

                // call the appropriate asm operation
                if (this->operation == '+')
                    std::cout << "add %rbx, %rax" << std::endl;
                else if (this->operation == '-')
                    std::cout << "sub %rbx, %rax" << std::endl;
                else if (this->operation == '*')
                    std::cout << "imul %rbx, %rax" << std::endl;
                else if (this->operation == '/')
                    std::cout << "idiv %rbx" << std::endl;

                // push result back onto the stack
                std::cout << "push %rax" << std::endl;
            }
            // assignment operation
            else {
                this->rhs->translate();                                         // simplify the expression on the right of the '='
                std::cout << "pop %rax" << std::endl;                           // retrieve latest value of expr from stack
                std::cout << "mov " << "%rax, " << this->lhs->data<< std::endl; // save the result to the variable member data
            }
        }
    }
} ExprNode;

typedef struct PrintNodeT : TreeNode {
    ExprNode*child;

    void translate() {
        child->translate();                          // simplify expression to be printed
        std::cout << "pop %rax" << std::endl;        // retrieve latest value stored from translate
        std::cout << "mov $0, %rcx" << std::endl;    // reset counter for expression's length (rcx)
        std::cout << "mov $10, %rbx" << std::endl;   // store the value 10 for value division to yield next digit
        std::cout << "call makeDigits" << std::endl; // jump to start of print sub-routines
    }
} PrintNode;