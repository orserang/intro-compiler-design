%{
#include <string>

// Needs to be included before y.tab.h
#include "Nodes.h"
#include "translator.tab.h"

#define YY_DECL extern "C" int yylex()

%}

%%

[ \t]            ;

[\n]            { return EOL; }

"PRINT" 		{ return PRINT; }

[0-9]+          { yylval.sVal = strdup(yytext); return NUM_I; }
[a-zA-Z][a-zA-Z0-9]* { yylval.sVal = strdup(yytext); return VARNAME; }

"="             { return EQUALS; }
"+"             { return ADD; }
"-"             { return SUB; }
"*"             { return MULT; }
"/"             { return DIV; }

"("             { return LPAREN; }
")"             { return RPAREN; }


%%
