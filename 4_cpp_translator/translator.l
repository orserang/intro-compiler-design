%{
#include <string>

// Needs to be included before y.tab.h
#include "Nodes.h"
#include "translator.tab.h"

#define YY_DECL extern "C" int yylex()

%}

/***
 These are the same token rules as before except
 with the addition of a PRINT keyword token for 
 returning variable values. This is preferred
 over the previous method of just having the 
 var name be the return command.
***/

%%

[ \t]            ;

[\n]            { return EOL; }


"PRINT" 		{ return PRINT; }

[0-9]+          { yylval.sVal = strdup(yytext); return NUM_I; }
[a-zA-Z][a-zA-Z0-9]* { yylval.sVal = strdup(yytext); return VARNAME; }

"="             { return EQUALS; }
"+"             { return ADD; }
"-"             { return SUB; }
"*"             { return MULT; }
"/"             { return DIV; }

"("             { return LPAREN; }
")"             { return RPAREN; }


%%
