#include <set>
#include <vector>
#include <list>

typedef struct TreeNodeT {
	char type;
    std::set<char*>* sources = new std::set<char*>();
    TreeNodeT* next;

    virtual void translate() =0;
    virtual void start_DFS() =0;

} TreeNode;

typedef struct ExprNodeT : TreeNode {

     char operation;
     char* data;
	 char* dest;

     ExprNodeT*lhs, *rhs;

     void translate() {
      	if (this->lhs==NULL && this->rhs==NULL)
		    // Base case: variable or integer value (both as string)
		      std::cout << "push " << this->data << std::endl;
	    else {
	    	// operations other than assign
	        if (this->operation != '=') {
	        		// retrieve the last two values from the stack
	        		// to perform the operation on
		        	this->rhs->translate();
		        	std::cout << "pop %rax" << std::endl;
		        	this->lhs->translate();
		        	std::cout << "pop %rbx" << std::endl;

		        	// call the appropriate asm operation
		        	if (this->operation == '+')
		        		std::cout << "add %rbx, %rax" << std::endl;
		        	else if (this->operation == '-')
		        		std::cout << "sub %rbx, %rax" << std::endl;
		        	else if (this->operation == '*')
		        		std::cout << "imul %rbx, %rax" << std::endl;
		        	else if (this->operation == '/')
		        		std::cout << "idiv %rbx" << std::endl;

		        	// push result back onto the stack
		        	std::cout << "push %rax" << std::endl;
		    }
		    // assignment operation
		    else {
		    	this->rhs->translate(); 			  							// simplify the expression on the right of the '='
		    	std::cout << "pop %rax" << std::endl; 							// retrieve latest value of expr from stack
		    	std::cout << "mov " << "%rax, " << this->lhs->data<< std::endl; // save the result to the variable member data
		    }
		}
     }

      /* Building set of source nodes only desired when the result is stored
		 (e.g. the ExprNode is an assignment operation)
      */
      void start_DFS() {
      	if (operation == '=') {
	        dest = lhs->data;
	        DFS_data(rhs, sources);
	    }
      }

      /* Recursively build the set of all source nodes required for rhs expression */
      void DFS_data(ExprNodeT* node, std::set<char *>* sources) {
	    if (node->lhs == NULL && node->rhs == NULL) {
	      if (node->data != NULL)
	        sources->insert(node->data);
	    }
	    else {
	      if (node->lhs != NULL)
	        DFS_data(node->lhs, sources);
	      if (node->rhs != NULL)
	        DFS_data(node->rhs, sources);
	    }
	}
} ExprNode;

typedef struct PrintNodeT : TreeNode {
      ExprNode*child;

      void translate() {
      	child->translate();							 // simplify expression to be printed
		std::cout << "pop %rax" << std::endl;        // retrieve latest value stored from translate
		std::cout << "mov $0, %rcx" << std::endl;    // reset counter for expression's length (rcx)
		std::cout << "mov $10, %rbx" << std::endl;   // store the value 10 for value division to yield next digit
		std::cout << "call makeDigits" << std::endl; // jump to start of print sub-routines
      }

      /* Build the set of sources for the expr to be printed 
         necessary for it reach its current state 
      */
      void start_DFS() {
      	child->DFS_data(child, sources);
      }
} PrintNode;

// 
typedef struct IfNodeT : TreeNode {

	  IfNodeT* partner;
	  int half;
	  int index;
	  std::vector<TreeNode*> *containedNodes = new std::vector<TreeNode*>();
	  bool in_single_block = false;

	  // TODO: evaluate comparison expression to jump between halves

      void translate() {
      	// label the branch block by index
      	std::cout << "block" << index << "_" << half << ":" << std::endl;
      	// define what branch does by collection of contained statements
      	for (int i=0; i<containedNodes->size(); ++i)
      		(*containedNodes)[i]->translate();
      	// exit branch statement
      	std::cout << "endblock" << index << ": " << std::endl;
      }

      // TODO: determine successors and precessors of block?
      // TODO: re-run make_CFG on just the containedNodes?

      void start_DFS() {

      }
} IfNode;

typedef struct CFGNodeT {
	  std::set<int> predecessors, successors;
      bool not_dead_code = false;
      std::list<CFGNodeT*>* children = new std::list<CFGNodeT*>();
} CFGNode;

typedef struct LogicNodeT : ExprNode {

	  char* compOp;

      void translate() {
      	std::cout << "LOGIC FOUND" << std::endl;
      }

      void start_DFS() {
      	std::cout << "START DFS FOR LOGIC" << std::endl;
      }
} LogicNode;