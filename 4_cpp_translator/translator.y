/***
 Purpose: Use the compiler to translate from the basic
 custom language to a cpp output that will further 
 compile and perform the basic arithmetic operations
 and use results stored in variables (e.g. PRINT or 
 further calculations).
***/


%{
#include <iostream>
#include <set>
#include <list>
#include <string>

#include "Nodes.h"

extern "C" int yylex();
extern "C" int yyparse();

std::set<std::string> all_variables;
std::list<TreeNode*> *lines = new std::list<TreeNode*>();

void yyerror(const char *s);
void createNewExpr(ExprNode* lhs, ExprNode* rhs, char oper, ExprNode* newNode);
void declare_vars();
void declare_imports();
void translate_lines(std::list<TreeNode*> *lines);

%}

/***
 Our return data types for the grammar rules include
 a TreeNode* to represent each line which is further
 inherited by ExprNode* and PrintNode* to separate
 how each is translated by a polymorphic call, 
 translate(). ExprNode is separated into lhs, rhs,
 and oper. PrintNode is separated into just its child.
***/

%union {
  ExprNode* eNode;
  PrintNode* pNode;
  TreeNode* tNode;
  char* sVal;
}

%token NUM_I
%token NUM_F
%token VARNAME
%token ADD SUB MULT DIV EQUALS
%token LPAREN RPAREN
%token WS
%token EOL
%token PRINT

%type<tNode> statement
%type<eNode> atomicExpr
%type<eNode> assign
%type<eNode> expr
%type<eNode> addSubExpr
%type<eNode> multDivExpr
%type<pNode> print

%type<sVal> NUM_I
%type<sVal> VARNAME
%type<sVal> PRINT

%%

line: statement EOL {
          // Since the return value of statement has to be a generic
          // TreeNode*, we have to recast the pointer to the more
          // specific children types.
          if ($1->type=='e')
            lines->push_back((ExprNode*) $1);
          else if ($1->type=='p') 
            lines->push_back((PrintNode*) $1);
       } line
   | /* This allows an empty line so the recursive definition can terminate */
   ;

statement: expr  {$1->type='e'; $$ = (TreeNode*) $1;}
     | print     {$1->type='p'; $$ = (TreeNode*) $1;}

expr: addSubExpr
    | assign

addSubExpr: addSubExpr ADD multDivExpr {
              ExprNode* newNode = new ExprNode;
              createNewExpr($1, $3, '+', newNode);
              $$ = newNode;
            }
          | addSubExpr SUB multDivExpr {
              ExprNode* newNode = new ExprNode;
              createNewExpr($1, $3, '-', newNode);
              $$ = newNode;
            }
          | multDivExpr

multDivExpr: multDivExpr MULT atomicExpr {
              ExprNode* newNode = new ExprNode;
              createNewExpr($1, $3, '*', newNode);
              $$ = newNode;
            }
          | multDivExpr DIV atomicExpr {
              if ($3 == 0) {
                std::cerr << "ERROR: Cannot divide by 0!" << std::endl;
                exit(1);
              }
              else {
                ExprNode* newNode = new ExprNode;
                createNewExpr($1, $3, '/', newNode);
                $$ = newNode;
              }
          }
          | atomicExpr

atomicExpr: VARNAME {
              ExprNode* newNode = new ExprNode;
              newNode->data = $1;
              $$ = newNode;
	      
      	      // add this variable to our name set
      	      all_variables.insert(std::string($1));
            }
          | NUM_I { 
              ExprNode* newNode = new ExprNode;
              newNode->data = $1;
              $$ = newNode; 
            }
          | LPAREN expr RPAREN {
            $$ = $2;
          }

assign: VARNAME EQUALS expr {
          ExprNode* var = new ExprNode;
          var->data = $1;
          ExprNode* newNode = new ExprNode;
          createNewExpr(var, $3, '=', newNode);
          $$ = newNode;

          all_variables.insert(std::string($1));
        }

print: PRINT expr {
         PrintNode* print = new PrintNode;
         print->child = $2;

         $$ = print;
       }

%%

int main(int argc, char **argv) {
  yyparse();

  declare_imports();
  declare_vars();

  translate_lines(lines);
}

/* Display error messages */
void yyerror(const char *s) {
  printf("ERROR: %s\n", s);
}

// A function to define main data for ExprNode struct
void createNewExpr(ExprNode* lhs, ExprNode* rhs, char oper, ExprNode* newNode) {
  newNode->lhs = lhs;
  newNode->operation = oper;
  newNode->rhs = rhs;
}

// Declares integer space for all variables 
// detected during the parsing
void declare_vars() {
  if (all_variables.size() > 0) {
    std::cout << "int ";
    auto iter_before_end = all_variables.end();
    --iter_before_end;
    auto iter=all_variables.begin();
    for (; iter!=iter_before_end; ++iter) {
      std::cout << *iter << ", ";
    }
    std::cout << *iter << ';' << std::endl;
  }
}

// Declares necessary cpp files in translated code
void declare_imports() {
  std::cout << "#include <iostream>" << std::endl;
}

// Iterates over our list of TreeNode representations
// line by line to yield cpp code.
void translate_lines(std::list<TreeNode*> *lines) {
  std::cout << "int main(int argc, char** argv) {" << std::endl;
  for (std::list<TreeNode*>::iterator iter = lines->begin(); iter != lines->end(); iter++) {
    (*iter)->translate();
  }
  std::cout << "}" << std::endl;
}
