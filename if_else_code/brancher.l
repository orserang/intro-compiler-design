%{
#include <iostream>
#include <string>

// Needs to be included before y.tab.h
#include "Nodes.h"
#include "brancher.tab.h"

#define YY_DECL extern "C" int yylex()

%}

%%

[ \t]            ; //ignore whitespace

[\n]            { return EOL; }


"PRINT" 		{ return PRINT; }
"IF"			{ return IF; }
"ELSE"			{ return ELSE; }

[0-9]+          { yylval.sVal = strdup(yytext); return NUM_I; }
[a-zA-Z][a-zA-Z0-9]* { yylval.sVal = strdup(yytext); return VARNAME; }

"="             { return EQUALS; }
"+"             { return ADD; }
"-"             { return SUB; }
"*"             { return MULT; }
"/"             { return DIV; }

"("             { return LPAREN; }
")"             { return RPAREN; }
"{"				{ return LBRACE; }
"}"				{ return RBRACE; }

"<"				{ return LESS; }
">"				{ return GREATER; }
"=="			{ return EQUALSS; }
"!="			{ return NOT_EQUALSS; }
"<="			{ return LESS_EQUALSS; }
">="			{ return GREATER_EQUALSS; }

"&&"			{ return AND; }
"||"			{ return OR; }


%%
