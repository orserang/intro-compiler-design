/***
 Purpose: Use the compiler to translate from the basic
 custom language to an asm output that will further 
 compile and perform the basic arithmetic operations
 and use results stored in variables (e.g. PRINT or 
 further calculations).
***/

%{
#include <iostream>
#include <set>
#include <list>
#include <cstring>

#include "Nodes.h"

extern "C" int yylex();
extern "C" int yyparse();

std::set<std::string> all_variables;
std::list<TreeNode*> *lines = new std::list<TreeNode*>();

void yyerror(const char *s);
void createNewExpr(ExprNode* lhs, ExprNode* rhs, char oper, ExprNode* newNode);
void declare_vars();
void declare_print();
void translate_lines(std::list<TreeNode*> *lines);

// Helper function to concatenate const char*
char*concatenate(const char*a, const char*b) {
  char*result = new char[strlen(a) + strlen(b)];
  strcpy(result, a);
  strcpy(result+strlen(a), b);
  return result;
}

%}

%union {
  ExprNode* eNode;
  PrintNode* pNode;
  TreeNode* tNode;
  char* sVal;
}

%token NUM_I
%token NUM_F
%token VARNAME
%token ADD SUB MULT DIV EQUALS
%token LPAREN RPAREN
%token WS

%token EOL
%token PRINT

%type<tNode> statement
%type<eNode> atomicExpr
%type<eNode> assign
%type<eNode> expr
%type<eNode> addSubExpr
%type<eNode> multDivExpr
%type<pNode> print

%type<sVal> NUM_I
%type<sVal> VARNAME
%type<sVal> PRINT

%%

line: statement EOL {
          if ($1->type=='e')
            lines->push_back((ExprNode*) $1);
          else if ($1->type=='p') 
            lines->push_back((PrintNode*) $1);
       } line
   | /* This allows an empty line so the recursive definition can terminate */
   ;

statement: expr  {$1->type='e'; $$ = (TreeNode*) $1;}
     | print     {$1->type='p'; $$ = (TreeNode*) $1;}

expr: addSubExpr
    | assign

addSubExpr: addSubExpr ADD multDivExpr {
              ExprNode* newNode = new ExprNode;
              createNewExpr($1, $3, '+', newNode);
              $$ = newNode;
            }
          | addSubExpr SUB multDivExpr {
              ExprNode* newNode = new ExprNode;
              createNewExpr($1, $3, '-', newNode);
              $$ = newNode;
            }
          | multDivExpr

multDivExpr: multDivExpr MULT atomicExpr {
              ExprNode* newNode = new ExprNode;
              createNewExpr($1, $3, '*', newNode);
              $$ = newNode;
            }
          | multDivExpr DIV atomicExpr {
              if ($3 == 0) {
                std::cerr << "ERROR: Cannot divide by 0!" << std::endl;
                exit(1);
              }
              else {
                ExprNode* newNode = new ExprNode;
                createNewExpr($1, $3, '/', newNode);
                $$ = newNode;
              }
          }
          | atomicExpr

atomicExpr: VARNAME {
              ExprNode* newNode = new ExprNode;
              newNode->data = $1;
              $$ = newNode;
        
              // add this variable to our name set
              all_variables.insert(std::string($1));
            }
          | NUM_I { 
              ExprNode* newNode = new ExprNode;
              newNode->data = concatenate("$", $1);
              $$ = newNode; 
            }
          | LPAREN expr RPAREN {
            $$ = $2;
          }

assign: VARNAME EQUALS expr {
          ExprNode* var = new ExprNode;
          var->data = $1;
          ExprNode* newNode = new ExprNode;
          createNewExpr(var, $3, '=', newNode);
          $$ = newNode;

          all_variables.insert(std::string($1));
        }

print: PRINT expr {
         PrintNode* print = new PrintNode;
         print->child = $2;

         $$ = print;
       }

%%

int main(int argc, char **argv) {
  yyparse();

  translate_lines(lines);
  declare_print();
  std::cout << "end: " << std::endl; // End of code label to not call sub-routines again
  declare_vars();

}

/* Display error messages */
void yyerror(const char *s) {
  fprintf(stderr, "ERROR: %s\n", s);
}

void createNewExpr(ExprNode* lhs, ExprNode* rhs, char oper, ExprNode* newNode) {
  newNode->lhs = lhs;
  newNode->operation = oper;
  newNode->rhs = rhs;
}

void declare_vars() {
  if (all_variables.size() > 0) {
    std::cout << std::endl << ".data " << std::endl;
    for (auto iter=all_variables.begin(); iter != all_variables.end(); ++iter) {
      std::cout << "    " << *iter << ":" << std::endl << "        .quad 0 " << std::endl;
    } 
    std::cout << std::endl; 
  }
}

void declare_print() {
  std::cout << "makeDigits: " << std::endl;      // label of makeDigits function
    std::cout << "xor %rdx, %rdx" << std::endl;  // clear rdx (dest of remainder)
    std::cout << "div %rbx" << std::endl;        // divide rax by 10
    std::cout << "mov %rax, %r8" << std::endl;   // save current result of div
    std::cout << "mov %rdx, %rax" << std::endl;  // save remainder of div
    std::cout << "add $0x30, %rax" << std::endl; // change value of div to ascii digit
    std::cout << "push %rax" << std::endl;       // push digit onto stack to be printed
    std::cout << "mov %r8, %rax" << std::endl;   // restore non-ascii value of rax
    std::cout << "inc %rcx" << std::endl;        // increment how many digits are made
    std::cout << "cmp $0, %rax" << std::endl;    // check if we're done making digits
    std::cout << "jnz makeDigits" << std::endl;  // move to next digit

    std::cout << "printDigits:" << std::endl;    // label of printDigits function
    std::cout << "pop %rax" << std::endl;        // get next ascii char to print
    std::cout << "mov %rsp, %r9" << std::endl;   // save current stack pointer position
    std::cout << "mov %rcx, %r10" << std::endl;  // save current number of digits 
    std::cout << "sub $8, %rsp" << std::endl;    // move stack pointer to next byte
    std::cout << "mov %al, (%rsp)" << std::endl; // move lowest byte of val to the stack pointer
    std::cout << "mov %rsp, %rsi" << std::endl;  // move stack value to the source index
    std::cout << "mov $1, %rax" << std::endl;    // move 1 for syscall (system write)
    std::cout << "mov $1, %rdi" << std::endl;    // move 1 to dest index (system out)
    std::cout << "mov $1, %rdx" << std::endl;    // move 1 for number of bytes written (1)
    std::cout << "syscall" << std::endl;         // print
    std::cout << "mov %r9, %rsp" << std::endl;   // restore rsp
    std::cout << "mov %r10, %rcx" << std::endl;  // restore rcx (number of digits left)
    std::cout << "dec %rcx" << std::endl;        // decrement number of digits left
    std::cout << "jnz printDigits" << std::endl; // continue print for more digits

    // print a '\n':

    // push '\n' onto stack:
    std::cout << "mov %rsp, %r9" << std::endl;   
    std::cout << "mov $10, %rax" << std::endl;   // move ascii val of \n
    std::cout << "sub $8, %rsp" << std::endl;
    std::cout << "mov %al, (%rsp)" << std::endl;
    std::cout << "mov $1, %rax" << std::endl;
    std::cout << "mov $1, %rdi" << std::endl;
    std::cout << "mov $1, %rdx" << std::endl;
    std::cout << "syscall" << std::endl;
    std::cout << "mov %r9, %rsp" << std::endl;

    std::cout << "ret" << std::endl;
} 

void translate_lines(std::list<TreeNode*> *lines) {
  std::cout << ".globl main" << std::endl << std::endl << "main:"<< std::endl;
  for (std::list<TreeNode*>::iterator iter = lines->begin(); iter != lines->end(); iter++) {
    std::cout << "    ";
    (*iter)->translate();
  }
  std::cout << "jmp end" << std::endl;
}
