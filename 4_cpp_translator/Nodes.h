#include <iostream>

typedef struct TreeNodeT {
	  char type;

      virtual void translate() =0;
} TreeNode;

typedef struct ExprNodeT : TreeNode {

      char operation;
      char* data;

      ExprNodeT*lhs, *rhs;

      void translate() {
      	if (this->lhs == NULL && this->rhs == NULL)
		    // Base case: variable or integer value (both as string)
		      std::cout << this->data;
	    else {
	    	// Expressions other than an assign
	        if (this->operation != '=') {

	        // Wrap in parentheses to respect order of operations
	        std::cout << "(";
	        if (this->lhs != NULL)
	          this->lhs->translate();

	        if (this->lhs != NULL && this->rhs != NULL)
	          std::cout << this->operation;

	        if (this->rhs != NULL)
	          this->rhs->translate();
	        std::cout << ")";
	      }
	      else {
	      	// Assign expressions
	        // lhs should be from a variable
	        if (this->lhs != NULL)
	          this->lhs->translate();

	        if (this->lhs != NULL && this->rhs != NULL)
	          std::cout << this->operation;

	        // rhs should be recursively resolved
	        // to a parenthesis bound expr respecting
	        // order of operations
	        if (this->rhs != NULL)
	          this->rhs->translate();
	        std::cout << ';' << std::endl;
	      }
	    }
      }
} ExprNode;

typedef struct PrintNodeT : TreeNode {
      ExprNode*child;

      void translate() {
      	std::cout << "std::cout << ";
      	this->child->translate(); // recursively resolve
      	std::cout << " << std::endl;" << std::endl;
      }
} PrintNode;